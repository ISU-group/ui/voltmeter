from abc import ABC, abstractmethod


class Observer:
    ...


class Subject(ABC):
    @abstractmethod
    def __init__(self) -> None:
        super().__init__()
        self._observers: list[Observer] = []

    def attach(self, observer: Observer) -> None:
        self._observers.append(observer)

    def detach(self, observer: Observer) -> None:
        self._observers.remove(observer)

    def notify(self) -> None:
        for observer in self._observers:
            observer.update(self)
