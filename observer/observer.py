from abc import ABC, abstractmethod


class Subject:
    ...


class Observer(ABC):
    @abstractmethod
    def __init__(self) -> None:
        super().__init__()

    @abstractmethod
    def update(self, subject: Subject) -> None:
        ...
