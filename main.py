import threading
from time import sleep

from voltmeter import Voltmeter
from voltmeter.plotter import Plotter

UP = True


def update_state(voltmeter: Voltmeter) -> None:
    while UP:
        voltmeter.update_state()
        voltmeter.notify()
        sleep(0.01)


voltmeter = Voltmeter()

t = threading.Thread(target=update_state, args=[voltmeter])
t.start()

plotter = Plotter(voltmeter)

UP = False
t.join()
