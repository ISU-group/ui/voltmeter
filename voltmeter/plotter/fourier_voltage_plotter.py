import dearpygui.dearpygui as dpg
import numpy as np

from voltmeter.plotter.plotter_interface import PlotterInterface


class FourierVoltagePlotter(PlotterInterface):
    def __init__(self) -> None:
        super().__init__()

        self._window_tag = None
        self._x_axis_tag = None
        self._y_axis_tag = None
        self._series_tag = None

    def setup(self, *args, **kwargs) -> None:
        self._window_tag = kwargs.get("window_tag")

        with dpg.group(parent=self._window_tag):
            with dpg.plot(label=type(self).__name__, width=-1, height=240):
                # optionally create legend
                dpg.add_plot_legend()

                self._x_axis_tag = dpg.add_plot_axis(dpg.mvXAxis, label="x")
                self._y_axis_tag = dpg.add_plot_axis(dpg.mvYAxis, label="y")

                self._series_tag = dpg.add_line_series(
                    x=[], y=[], parent=self._y_axis_tag
                )

    def plot(self, x, y) -> None:
        if dpg.is_item_ok(self._series_tag):
            sp = np.fft.rfft(np.array(y))
            freq = np.fft.rfftfreq(len(y))

            dpg.set_value(self._series_tag, [list(freq), list(sp.real)])
            dpg.fit_axis_data(self._x_axis_tag)
