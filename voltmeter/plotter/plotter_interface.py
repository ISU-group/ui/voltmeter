from abc import ABC, abstractmethod


class PlotterInterface(ABC):
    @abstractmethod
    def __init__(self) -> None:
        super().__init__()

    @abstractmethod
    def setup(self, *args, **kwargs) -> None:
        ...

    @abstractmethod
    def plot(self, x, y) -> None:
        ...
