import dearpygui.dearpygui as dpg
from collections import deque

from voltmeter.plotter.constants import DEFAULT_QUEUE_SIZE
from voltmeter.plotter.plotter_interface import PlotterInterface


class MeanVoltagePlotter(PlotterInterface):
    MIN_TIME_PERIOD = 1.0  # seconds
    TIME_STEP = 0.025

    def __init__(self) -> None:
        super().__init__()
        self._period = MeanVoltagePlotter.MIN_TIME_PERIOD

        self._buffer = []
        self._start_time = None

    def setup(self, *args, **kwargs) -> None:
        self._parse_args(*args, **kwargs)

        with dpg.group(parent=self._window_tag):
            with dpg.plot(label=type(self).__name__, width=-1, height=240):
                # optionally create legend
                dpg.add_plot_legend()

                self._x_axis_tag = dpg.add_plot_axis(dpg.mvXAxis, label="x")
                self._y_axis_tag = dpg.add_plot_axis(dpg.mvYAxis, label="y")

                self._series_tag = dpg.add_line_series(
                    x=[], y=[], parent=self._y_axis_tag
                )

            with dpg.group(horizontal=True):
                dpg.add_text("Time period: ")
                dpg.add_input_float(
                    width=180,
                    default_value=self._period,
                    step=MeanVoltagePlotter.TIME_STEP,
                    min_value=MeanVoltagePlotter.MIN_TIME_PERIOD,
                    callback=lambda sender, app_data, user_data: self._update_period(
                        app_data
                    ),
                )

    def _parse_args(self, *args, **kwargs) -> None:
        self._window_tag = kwargs.get("window_tag")
        self._max_size = kwargs.get("max_size", DEFAULT_QUEUE_SIZE)

        self._mean_series = deque([], self._max_size)
        self._time_series = deque([], self._max_size)

    def _update_period(self, period) -> None:
        self._period = period

        self._mean_series.clear()
        self._time_series.clear()

    def plot(self, x, y) -> None:
        self.update(x, y)

        if dpg.is_item_ok(self._series_tag):
            dpg.set_value(
                self._series_tag, [list(self._time_series), list(self._mean_series)]
            )
            dpg.fit_axis_data(self._x_axis_tag)

    def update(self, x, y) -> None:
        if len(x) == 0 and len(y) == 0:
            return

        if self._start_time is None:
            self._start_time = x[0]

        if x[-1] - self._start_time >= self._period:
            self._mean_series.append(sum(self._buffer) / len(self._buffer))
            self._time_series.append(x[-1])

            self._buffer = self._buffer[1:]
            self._start_time = x[1]

        self._buffer.append(y[-1])
