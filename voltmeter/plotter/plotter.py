import time
import dearpygui.dearpygui as dpg
from collections import deque

from observer import Observer, Subject

from voltmeter.voltmeter import Voltmeter
from voltmeter.plotter.constants import DEFAULT_QUEUE_SIZE
from voltmeter.plotter.voltage_plotter import VoltagePlotter
from voltmeter.plotter.plotter_interface import PlotterInterface
from voltmeter.plotter.mean_voltage_plotter import MeanVoltagePlotter
from voltmeter.plotter.fourier_voltage_plotter import FourierVoltagePlotter


class Plotter(Observer, PlotterInterface):
    def __init__(self, voltmeter: Voltmeter, *args, **kwargs) -> None:
        if "window_tag" not in kwargs:
            kwargs["window_tag"] = "voltmeter-plotter"

        self._window_tag = kwargs.get("window_tag")
        self._window_name = kwargs.get("window_name", "voltmeter-plotter")

        self._plotters: list[PlotterInterface] = [
            VoltagePlotter(),
            MeanVoltagePlotter(),
            FourierVoltagePlotter(),
        ]

        self._max_size = kwargs.get("queue_size", DEFAULT_QUEUE_SIZE)
        self._series = deque([], self._max_size)
        self._start_time = time.time()
        self._time_series = deque([], self._max_size)

        self._voltmeter = voltmeter
        self._voltmeter.attach(self)

        self.setup()

    def __del__(self) -> None:
        self._voltmeter.detach(self)

    def setup(self, *args, **kwargs) -> None:
        dpg.create_context()

        with dpg.window(label=self._window_name, tag=self._window_tag):
            for i in range(len(self._plotters)):
                self._plotters[i].setup(
                    window_tag=self._window_tag,
                    height=-1,
                    width=-1,
                    max_size=self._max_size,
                )

                if i + 1 != len(self._plotters):
                    dpg.add_spacer(height=10)

        dpg.create_viewport(title=self._window_name)
        dpg.setup_dearpygui()
        dpg.show_viewport()
        dpg.set_primary_window(self._window_tag, True)
        dpg.start_dearpygui()
        dpg.destroy_context()

    def update(self, subject: Subject) -> None:
        if self._voltmeter == subject:
            self._time_series.append(time.time() - self._start_time)
            self._series.append(self._voltmeter.value)
            self.plot(list(self._time_series), list(self._series))

    def plot(self, x, y) -> None:
        for plotter in self._plotters:
            plotter.plot(x, y)
