from typing import Any
import numpy as np
from queue import Queue
from time import sleep

from observer import Observer, Subject


class Voltmeter(Subject):
    POSSIBLE_VOLTAGE = [3.3, 5, 12, 24, 230]
    DEFAULT_VOLTAGE = POSSIBLE_VOLTAGE[0]

    def __init__(self) -> None:
        super().__init__()
        self._value = 0
        self._mean_voltage = Voltmeter.DEFAULT_VOLTAGE
        self._std = self._mean_voltage * 0.05

    def update_state(self) -> None:
        self._value = np.random.normal(self._mean_voltage, self._std)

    @property
    def value(self):
        return self._value
